import React, { Component } from 'react'
import { TeamMembar } from './TeamMembar';

export class TeamList extends Component {

  state = {
    mousePosition: null,
    isClicked: false,
  }
  
  componentWillMount() {
    document.onmousemove = ({ pageX, pageY }) => {
      this.setState({
        isClicked: false,
        mousePosition: {
          x: pageX,
          y: pageY,
        }
      })
    }
  }

  handleClick = _ => this.setState({isClicked: true})
  
  render() {
    const { teamPhotos } = this.props
    const { mousePosition, isClicked } = this.state
    return (
      <div className="flex flex-wrap">
        {teamPhotos.map((memberPhotos, i) => (
          <TeamMembar
            key={i}
            memberPhotos={memberPhotos}
            mousePosition={mousePosition}
            isClicked={isClicked}
            onClick={this.handleClick}
          />
        ))}
      </div>
    )
  }

}

export default TeamList
