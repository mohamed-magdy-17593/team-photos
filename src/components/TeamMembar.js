import React, { Component } from 'react'

const getDirection = ({ img, mousePosition, isClicked }) => 
  mousePosition
    ? getDirectionUsingPosition(img, mousePosition, isClicked)
    : 'fancyFront'

const getDirectionUsingPosition = (img, mousePosition, isClicked) => {
  const xlen = (mousePosition.x - img.position.x) || 0.01
  const ylen = (mousePosition.y - img.position.y) || 0.01
  const isInRadius = Math.abs(xlen) < img.radius && Math.abs(ylen) < img.radius
  if (isClicked) {
    if (isInRadius) {
      return 'fancyFront'
    } else {
      return 'down'
    }
  } else {
    if (isInRadius) {
      return 'front'
    } else {
      return getDirectionByAngle(getAngle(xlen, ylen))
    }
  }
}

const getAngle = (xlen, ylen) => {
  const baseAngle = Math.abs(Math.atan(ylen / xlen) * 180 / Math.PI)
  switch (`${xlen > 0 ? '+' : '-'}${ylen > 0 ? '+' : '-'}`) {
    case '++': return baseAngle
    case '-+': return 180 - baseAngle
    case '--': return 180 + baseAngle
    case '+-': return 360 - baseAngle
    default: return 0
  }
}

const getDirectionByAngle = angle => {
  const directions = [
    'right',
    'downRight',
    'down',
    'downLeft',
    'left',
    'upLeft',
    'up',
    'upRight',
  ]
  const newEasyAngle = (angle + 22.5) % 360
  for (let testAngle = 45; testAngle < 361; testAngle = testAngle + 45) {
    const direction = directions.shift()      
    if (newEasyAngle < testAngle) {
      return direction
    }
  }
}

export class TeamMembar extends Component {
  get imgBounding() {
    if (!this.imgContainer) return null
    return this.imgContainer.getBoundingClientRect()
  }
  get imgRadius() {
    if (!this.imgContainer) return null
    const { top, bottom } = this.imgBounding
    return Math.abs((top - bottom) / 2)
  }
  get imgCenterPosition() {
    if (!this.imgContainer) return null
    const { top, right, bottom, left } = this.imgBounding
    return {
      x: (right + left) / 2,
      y: (top + bottom) / 2,
    }
  }
  render() {
    const { memberPhotos, mousePosition, onClick, isClicked } = this.props
    const img = [{
      img: { position: this.imgCenterPosition, radius: this.imgRadius },
      mousePosition,
      isClicked,
    }].map(getDirection)
      .map(direction => memberPhotos[direction])[0]
    return (
      <div
        className="w3 h3 br-pill ma3 overflow-hidden"
        ref={ref => this.imgContainer = ref}
        onClick={onClick}
      >
        <img
          src={img}
          alt=""
        />
      </div>
    )
  }
}

export default TeamMembar
