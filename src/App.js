import React, { Component } from 'react';
import { TeamList } from './components/TeamList';

class App extends Component {

  state = {
    teamPhotos: Array(8*5).fill({
      up: 'images/team-members/up.png',
      down: 'images/team-members/down.png',
      right: 'images/team-members/right.png',
      left: 'images/team-members/left.png',
      upLeft: 'images/team-members/up-left.png',
      upRight: 'images/team-members/up-right.png',
      downLeft: 'images/team-members/down-left.png',
      downRight: 'images/team-members/down-right.png',
      front: 'images/team-members/front.png',
      fancyFront: 'images/team-members/fancy-front.png',
    })
  }

  render() {
    const { teamPhotos } = this.state
    return (
      <div>
        <h1 className="f1 tc fw1">
          Our Team
        </h1>
        <section className="mw7 center">
          <TeamList
            teamPhotos={teamPhotos}
          />
        </section>
      </div>
    )
  }
}

export default App;
